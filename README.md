# syspass-exporter

A command-line tool to connect to a `syspass` database, read all
passwords (called "accounts" in `syspass` parlance), decrypt the
password & export the data as JSON.

## Warning

This isn't a ready-made solution. But it's so short that you can
easily adjust it to your needs.

## Requirements

The script assumes the database runs on `localhost` & is called
`syspass`, accessed by a user named `syspass`. If that doesn't match
your situation, adjust the `new mysqli(…)` call accordingly.

Apart from that:

• PHP (the command-line version) with `mysqli` & `JSON` support
• `composer`

## Usage

```sh
git clone https://gitlab.com/linetservices/syspass-export

cd syspass-export
export DATABASE_PASSWORD='<secret database password here>'
export MASTER_PASSWORD='<secret syspass master password here>'

composer install

php ./export.php | tee > export.json
```

The `tee` is only there so that you can see that something's
happening. Decrypting each account password takes roughly 1s. This
incremental output is the only reason the script does part of the JSON
output itself instead of using `json_encode()` on a whole array.

## License

Written by Moritz Bunkus, LINET Services GmbH, Germany.

Licensed under the Mozilla Public License 2.0 (see file
[LICENSE](LICENSE)).
