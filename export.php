#!/usr/bin/env php
<?php

require 'vendor/autoload.php';

use Defuse\Crypto\Crypto;
use Defuse\Crypto\KeyProtectedByPassword;
use Defuse\Crypto\Key;

$passwords = array();

foreach (array("DATABASE_PASSWORD", "MASTER_PASSWORD") as $key) {
    $passwords[$key] = getenv($key);
    if (!$passwords[$key]) {
        echo("env var " . $key . " must be set\n");
        exit(1);
    }
}

$sql_read_all = "
SELECT cl.name AS client, ca.name AS category,
  a.name AS account, a.login, a.url, a.pass, a.`key`, a.notes
FROM Account a
LEFT JOIN Client   cl ON (a.clientId   = cl.id)
LEFT JOIN Category ca ON (a.categoryId = ca.id)
";

$db = new mysqli("localhost", "syspass", $passwords["DATABASE_PASSWORD"], "syspass");
if (!$db) {
    echo("no DB connection\n");
    exit(1);
}

$result = $db->query($sql_read_all);
$entries = array();
$first = true;

echo("[");

foreach ($result as $row) {
    $locked_key = $row["key"];
    $unlocked_key = KeyProtectedByPassword::loadFromAsciiSafeString($locked_key)->unlockKey($passwords["MASTER_PASSWORD"]);

    $row["password"] = Crypto::decrypt($row["pass"], $unlocked_key);
    unset($row["pass"]);
    unset($row["key"]);

    if ($first) {
        $first = false;
    } else {
        echo(",");
    }

    echo("\n  " . json_encode($row));
}

echo("\n]\n");

// Local Variables:
// c-basic-offset: 4
// End:
